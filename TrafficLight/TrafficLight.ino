#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         9 
#define SS_PIN          10
#define PWR_PIN         7

#define RED_PID         2
#define YELLOW_PID      3
#define GREEN_PID       4

MFRC522 mfrc522(SS_PIN, RST_PIN);

void setup() {
  //Beacause arduino pro mini doesn't hava 3.3v output
  pinMode(PWR_PIN, OUTPUT);
  analogWrite(PWR_PIN, 168);

  pinMode(RED_PID, OUTPUT);
  pinMode(YELLOW_PID, OUTPUT);
  pinMode(GREEN_PID, OUTPUT);

  digitalWrite(RED_PID, HIGH); 
  
  Serial.begin(9600);
  while (!Serial);
  SPI.begin();
  mfrc522.PCD_Init();
  mfrc522.PCD_DumpVersionToSerial();
  Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
}

void loop() {
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  startGreen();
}

void startGreen(){
  lightsOff();
  digitalWrite(GREEN_PID, HIGH);
  delay(3000);
  startYellow();
}

void startYellow(){
  lightsOff();
  digitalWrite(YELLOW_PID, HIGH); 
  delay(800);
  startRed();
}

void startRed(){
  lightsOff();
  digitalWrite(RED_PID, HIGH); 
}

void lightsOff(){
  digitalWrite(RED_PID, LOW);
  digitalWrite(YELLOW_PID, LOW);
  digitalWrite(GREEN_PID, LOW); 
}


