#define LEFT_ENBLE    2
#define LEFT_BACK     3
#define LEFT_FRONT    5
#define RIGHT_ENABLE  7
#define RIGHT_BACK    6
#define RIGHT_FRONT   9
#define SIGNAL_LIGHT  10
#define LIGHTS_LEFT   11
#define LIGHTS_RIGHT  12
#define BUZZER        13

int frontLightsState = LOW;
int signalLightState = LOW;

String message;

unsigned long blockUntil;

void setup()
{
  pinMode(LEFT_ENBLE, OUTPUT);
  pinMode(LEFT_BACK, OUTPUT);
  pinMode(LEFT_FRONT, OUTPUT);
  pinMode(RIGHT_ENABLE, OUTPUT);
  pinMode(RIGHT_BACK, OUTPUT);
  pinMode(RIGHT_FRONT, OUTPUT);
  pinMode(SIGNAL_LIGHT, OUTPUT);
  pinMode(LIGHTS_LEFT, OUTPUT);
  pinMode(LIGHTS_RIGHT, OUTPUT);
  pinMode(BUZZER, OUTPUT);

  digitalWrite(LEFT_ENBLE, HIGH);
  digitalWrite(RIGHT_ENABLE, HIGH);
  
  Serial.begin(115200);
}

void loop()
{
  while(Serial.available())
  {
    char value = char(Serial.read());
    Serial.println(value); 
    switch(value){
      case '0':
        stopBuzzer();
        stop();
        break;
      case '1':
        goFront(150);
        break;
      case '2':
        goBack(150);
        break;
      case '3':
        goLeft(150);
        break;
      case '4':
        goRight(150);
        break;
      case '6':
        startBuzzer();
        break;
      case '7':
        if(millis() > blockUntil){
          blockUntil = millis() + 100;
          switchFrontLights();
        }
        break;
    }
    delay(1);
  }
  if(!Serial.available())
  {
    stopBuzzer();
    stop();
  }
}

void goLeft(int speed){
  digitalWrite(LEFT_BACK, speed);
  digitalWrite(RIGHT_BACK, LOW);

  analogWrite(LEFT_FRONT, LOW);
  analogWrite(RIGHT_FRONT, speed);
}

void goRight(int speed){
  digitalWrite(LEFT_BACK, LOW);
  digitalWrite(RIGHT_BACK, speed);

  analogWrite(LEFT_FRONT, speed);
  analogWrite(RIGHT_FRONT, LOW);
}

void goFront(int speed){
  digitalWrite(LEFT_BACK, LOW);
  digitalWrite(RIGHT_BACK, LOW);

  analogWrite(LEFT_FRONT, speed);
  analogWrite(RIGHT_FRONT, speed);
}


void goBack(int speed){
  digitalWrite(LEFT_FRONT, LOW);
  digitalWrite(RIGHT_FRONT, LOW);

  analogWrite(LEFT_BACK, speed);
  analogWrite(RIGHT_BACK, speed);
}

void stop(){
  digitalWrite(LEFT_BACK, LOW);
  digitalWrite(RIGHT_BACK, LOW);
  
  digitalWrite(LEFT_FRONT, LOW);
  digitalWrite(RIGHT_FRONT, LOW);
}

void switchFrontLights(){
  frontLightsState = !frontLightsState;
  digitalWrite(LIGHTS_LEFT, frontLightsState);
  digitalWrite(LIGHTS_RIGHT, frontLightsState);
}

void switchSignalLight(){
  signalLightState = !signalLightState;
  digitalWrite(SIGNAL_LIGHT, signalLightState);
}

void startBuzzer(){
  digitalWrite(BUZZER, HIGH);
}

void stopBuzzer(){
  digitalWrite(BUZZER, LOW);
}


    
